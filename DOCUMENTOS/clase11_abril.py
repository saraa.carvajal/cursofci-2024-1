#Calcular la probabilidad de colisión de dos partículas en un colisionador 
"""Experimental Framework (LHC):
The Large Hadron Collider (LHC) is a machine that accelerates and collides
with protons and ions

The compact Muon Solenoid (CMS) experiment is one of two large general-purpose particle physics detectors built on the LHC

* Search for the higgs boson, extra dimensions, and particles that could make up
* 21 meters long, 15 m in diameter and weighs about 14000 tons
* More than 4.000 people, representing 206 scientific institutes and 47 countries

*The interaction point*:
* The tracker
* The electromagnetic calorimeter
* The hadronic calorimeter
* The magnet 
* The muon detectors 
"""

import numpy as np 

n= 6
def rand_particless(n):
    x,y= np.random.randint(1,n)
    return x,y



