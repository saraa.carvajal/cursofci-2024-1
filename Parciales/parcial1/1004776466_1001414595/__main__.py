from classes import RKGA
from functions import dif_eq, lorenz


if __name__ == '__main__':
    '''
    This part of code is made to solve the lorenz problem using the classes.py with RKG and RKGA.
    '''

    lorenzCosntantValues = [[10,28,8/3],[10,28,8/3],[16,45,4],[16,45,4]]  #sigma, rho, B for each case
    initialCondition = [[0.,[0.,0.,0.]],[0.,[0.,0.01,0.]],[0.,[0.,0.,0.]],[0.,[0.,0.001,0.]]]   # [t0[x0,y0,z0]] for each case
    names = ['case1_Lorentz','case2_Lorentz','case3_Lorentz','case4_Lorentz'] #names to solve each plot
    t_final = 100  #final intration time
    
    #Running for all 4 proposed scenarios 
    for k in range(4):
        problem = RKGA(lorenz, initial_conditions=initialCondition[k],x_final=t_final,step=0.001,func_params=lorenzCosntantValues[k])
        problem.soluciones()
        problem.plot3D(names[k],'Lorentz atractor',['sigma','rho','Beta'])





