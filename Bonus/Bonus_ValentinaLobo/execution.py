from tiroparabolico import movParabolico
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    angle = np.pi/4 #Angulo de inclinacion, debe estar en radianes (float)
    vel_i = 5 #velocidad inicial (float, int)
    x0 = 0 #posicion en x incial (float, int)
    y0 = 0#posicion en y inicial (float, int)
    g = 9.8 #gravedad, (float positivo)


    cañon = movParabolico(angle, vel_i, x0, y0, g)
    tvuelo = cañon.tiempomax()
    t = np.linspace(0, tvuelo, 100)
    x, y = cañon.trayectoria(t)
    plt.plot(x, y)
    plt.title(f"Trayectoria de un tiro parabolico con: \n velocidad incial de {cañon.vo} m/s e inclinación {cañon.alpha:.2f} radianes")
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.grid()
    plt.show()
    
   